package com.superpet.weixin.api.pet;

import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.SqlPara;
import com.superpet.common.kits.DateKit;
import com.superpet.common.kits.NumberKit;
import com.superpet.common.model.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PetService {

    public static final PetService me = new PetService();

    public static final Integer PAGE_SIZE = 4;

    public List<Pets> getUserPetList(Long userId){
        SqlPara sqlPara = Pets.dao.getSqlPara("pets.getUserPetList",userId);
        return Pets.dao.find(sqlPara);
    }

    public Page<Pets> getUserPetListPage(Integer pageNumber, Long userId){
        SqlPara sqlPara = Pets.dao.getSqlPara("pets.getUserPetList",userId);
        return Pets.dao.paginate(pageNumber, PAGE_SIZE, sqlPara);
    }

    public Pets getPetById(Long petId){
        SqlPara sqlPara = Pets.dao.getSqlPara("pets.getPetById",petId);
        return Pets.dao.findFirst(sqlPara);
    }

    public List<PetPic> getUserPetPicList(Long petId, Integer limit){
        SqlPara sqlPara = PetPic.dao.getSqlPara("pets.getUserPetPicList",petId, limit);
        return PetPic.dao.find(sqlPara);
    }

    public PetFeedTotal getPetFeedTotal(Long userId, Long petId){
        SqlPara sqlPara = PetFeedTotal.dao.getSqlPara("pets.getPetFeedTotal",userId, petId);
        return PetFeedTotal.dao.findFirst(sqlPara);
    }

    public PetFeedLog getTodayFeedPet(Long petId, Long userId, String feedDate){
        SqlPara sqlPara = PetFeedLog.dao.getSqlPara("pets.getTodayFeedPet",petId, userId, feedDate);
        return PetFeedLog.dao.findFirst(sqlPara);
    }

    public List<PetFeedTotal> getUserPetFeedTotalList(Long petId){
        SqlPara sqlPara = PetFeedTotal.dao.getSqlPara("pets.getUserPetFeedTotalList",petId);
        return PetFeedTotal.dao.find(sqlPara);
    }

    public List<PetFeedLog> getUserPetFeedByDayList(Long petId, String feedDate){
        SqlPara sqlPara = PetFeedLog.dao.getSqlPara("pets.getUserPetFeedByDayList",petId, feedDate);
        return PetFeedLog.dao.find(sqlPara);
    }

    public PetHouse getPetHouse(Long userId, Long petId){
        SqlPara sqlPara = PetHouse.dao.getSqlPara("pets.getPetHouse",userId, petId);
        return PetHouse.dao.findFirst(sqlPara);
    }

    public List<PetHouse> getPetHouseList(Long userId){
        SqlPara sqlPara = PetHouse.dao.getSqlPara("pets.getPetHouseList",userId);
        return PetHouse.dao.find(sqlPara);
    }

    public List<PetFeedLog> getMyFeedPetList(Long userId){
        SqlPara sqlPara = PetFeedLog.dao.getSqlPara("pets.getMyFeedPetList", userId);
        return PetFeedLog.dao.find(sqlPara);
    }

    public List<PetFeedLog> getFeedMyPetList(Long userId){
        SqlPara sqlPara = PetFeedLog.dao.getSqlPara("pets.getFeedMyPetList",userId);
        return PetFeedLog.dao.find(sqlPara);
    }

    public Long savePetPic(Long petId, String picPath){
        PetPic pic = new PetPic();
        pic.setPetid(petId);
        pic.setPicPath(picPath);
        pic.setUpdateTime(DateKit.toTimeStr(new Date()));
        pic.save();
        return pic.getId().longValue();
    }

    public void deletePetPic(String petPicIds){
        String[] ids = petPicIds.split(",");
        List<Long> list = new ArrayList<Long>();
        for(String id: ids){
            list.add(Long.parseLong(id));
        }
        SqlPara sqlPara = Db.getSqlPara("pets.deletePetPic", Kv.by("idList",list));
        System.out.println(sqlPara.getSql());
        Db.update(sqlPara);
    }

    public String getImgPathPrefix(){
        return KeyValue.dao.findFirst("select pvalue from p_key_value where pkey='IMG_PATH_PREFIX' limit 1").getPvalue();
    }

    public String getPetCardBg(){
        SqlPara sqlPara = PetCardBg.dao.getSqlPara("pets.getPetCardBg");
        PetCardBg bg = PetCardBg.dao.findFirst(sqlPara);
        if(bg!=null){
            return bg.getCardBg();
        }else{
            return null;
        }
    }

    public List<PetCardWords> getCardWords123(){
        SqlPara sqlPara = PetCardWords.dao.getSqlPara("pets.getCardWords123");
        return PetCardWords.dao.find(sqlPara);
    }

    public String[] getCardRandomWords(){
        List<PetCardWords> words = this.getCardWords123();
        List<PetCardWords> masterWords = new ArrayList<>();
        List<PetCardWords> petWords = new ArrayList<>();
        List<PetCardWords> blessingWords = new ArrayList<>();
        for(PetCardWords w: words){
            if(w.getCardWordType()==1){
                masterWords.add(w);
            }else if(w.getCardWordType()==2){
                petWords.add(w);
            }else if(w.getCardWordType()==3){
                blessingWords.add(w);
            }
        }
        String[] result = new String[3];
        result[0] = masterWords.get(NumberKit.getFullRandom(masterWords.size())).getCardWords();
        result[1] = petWords.get(NumberKit.getFullRandom(petWords.size())).getCardWords();
        result[2] = blessingWords.get(NumberKit.getFullRandom(blessingWords.size())).getCardWords();
        return result;
    }

    public Pets getUserRandomPet(Long userId){
        List<Pets> pets = this.getUserPetList(userId);
        return pets.get(NumberKit.getFullRandom(pets.size()));
    }

}
